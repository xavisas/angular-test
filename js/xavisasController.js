/*Global Controller */

/**
 * The main controller for the app. 
 */
angular.module('xavisasList').controller('xavisasController', function xavisasController($scope) {
    $scope.todoList = [];

    $scope.newTodo = "";

    $scope.addTodo = function () {
        var newTodo = {
            action: $scope.newTodo,
            completed: false
        };

        if (newTodo.action === "") {
            return;
        }

        $scope.todoList.push(newTodo);
        $scope.newTodo="";
    };

});