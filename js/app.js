/*Main JS App*/

/**
 * The main app module. Here we include 
 * all module dependencies and
 * the configuration of the app.
 *
 * @type {angular.module}
 */
angular.module('xavisasList', ['ngRoute'])
       .config(function () {

});